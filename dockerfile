FROM node:14.15.1-slim as node
WORKDIR /app
COPY package.json /app/
RUN npm i npm@latest -g
RUN npm install
COPY ./ /app/
ARG env=prod
RUN npm run build

FROM nginx:1.17
COPY --from=node /app/dist/sgprev /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf