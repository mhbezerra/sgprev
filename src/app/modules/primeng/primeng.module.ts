import { NgModule } from '@angular/core';

import { StepsModule } from 'primeng/steps';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { AccordionModule } from 'primeng/accordion';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';

const primengModules: any[] = [
  StepsModule,
  FileUploadModule,
  PanelModule,
  MessagesModule,
  MessageModule,
  AccordionModule,
  CalendarModule,
  CardModule,
  DialogModule,
  ButtonModule,
  ConfirmDialogModule,
  DropdownModule,
  InputTextModule,
  RadioButtonModule,
  ScrollPanelModule,
  TooltipModule,
  TableModule,
  TabViewModule,
  ToastModule,
  ToolbarModule
];

@NgModule({
  declarations: [],
  imports: [
    primengModules    
  ],
  exports: [primengModules]
})
export class PrimengModule { }
