import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-beneficio',
  templateUrl: './beneficio.component.html',
  styleUrls: ['./beneficio.component.css']
})
export class BeneficioComponent implements OnInit {

  constructor(private router: Router) { }

  items: MenuItem[];
  public activeIndex = 0;

  ngOnInit() {
    this.items = [
      {label: 'Beneficiário', routerLink: 'beneficiario'},
      {label: 'Identificação', routerLink: 'identificacao'},
      {label: 'Vida funcional', routerLink: 'vida-funcional'},
      {label: 'Contagem de tempo', routerLink: 'contagem-tempo'},
      {label: 'Remuneração/Proventos', routerLink: 'remuneracao-proventos'}
    ];
  }

  goNext() {
    this.goToRouter(this.items[++this.activeIndex]);
  }

  goPrev() {
    this.goToRouter(this.items[--this.activeIndex]);
  }

  goToRouter(item: MenuItem) {
    this.router.navigate(['incluirBeneficio/'+item.routerLink]);
  }
}
