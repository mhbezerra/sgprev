import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemuneracaoProventosComponent } from './remuneracao-proventos.component';

describe('RemuneracaoProventosComponent', () => {
  let component: RemuneracaoProventosComponent;
  let fixture: ComponentFixture<RemuneracaoProventosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemuneracaoProventosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemuneracaoProventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
