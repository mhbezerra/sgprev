import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-manter-beneficio',
  templateUrl: './manter-beneficio.component.html',
  styleUrls: ['./manter-beneficio.component.scss']
})
export class ManterBeneficioComponent implements OnInit {

  constructor() { }

  items: MenuItem[];

  ngOnInit(): void {
    this.items = [
      {
        label: 'Beneficiario',
        icon: 'pi pi-user',
        command: () => {
          this.atualizaExibicao(0);
        }
      },
      {
        label: 'Identificação',
        icon: 'pi pi-folder-open',
        command: () => {
          this.atualizaExibicao(1);
        }
      },
      {
        label: 'Vida Funcional',
        icon: 'pi pi-folder-open',
        command: () => {
          this.atualizaExibicao(2);
        }
      },
      {
        label: 'Contagem de tempo',
        icon: 'pi pi-folder-open',
        command: () => {
          this.atualizaExibicao(3);
        }
      },
      {
        label: 'Remuneração/Proventos',
        icon: 'pi pi-folder-open',
        command: () => {
          this.atualizaExibicao(4);
        }
      }
    ]
  }

  tramitar() {
    console.log("tramitar");
  }

  atualizaExibicao(index: number) {
    console.log(index);
  }

}
