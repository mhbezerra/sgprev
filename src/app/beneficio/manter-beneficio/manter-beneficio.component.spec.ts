import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManterBeneficioComponent } from './manter-beneficio.component';

describe('ManterBeneficioComponent', () => {
  let component: ManterBeneficioComponent;
  let fixture: ComponentFixture<ManterBeneficioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManterBeneficioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManterBeneficioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
