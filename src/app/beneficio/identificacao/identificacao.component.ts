import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-identificacao',
  templateUrl: './identificacao.component.html',
  styleUrls: ['./identificacao.component.css']
})
export class IdentificacaoComponent implements OnInit {

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
  }

  uploadedFiles: File[] = [];

  onUpload(event) {
    for(let file of event.files) {
        const fileAux: File = file;
        this.uploadedFiles.push(fileAux);
        /*
        const blob: Blob = new Blob([fileAux], { type: 'application/pdf' });
        const objBlob = URL.createObjectURL(blob);
        window.open(objBlob);
        */
    }
    this.messageService.add({severity: 'success', summary: 'Mensagem', detail: 'Upload realizado com sucesso', life: 6000});
  }

}
