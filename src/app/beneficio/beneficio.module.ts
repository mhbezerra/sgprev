import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeneficiarioComponent } from './beneficiario/beneficiario.component';
import { IdentificacaoComponent } from './identificacao/identificacao.component';
import { VidaFuncionalComponent } from './vida-funcional/vida-funcional.component';
import { ContagemTempoComponent } from './contagem-tempo/contagem-tempo.component';
import { RemuneracaoProventosComponent } from './remuneracao-proventos/remuneracao-proventos.component';
import { ManterBeneficioComponent } from './manter-beneficio/manter-beneficio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { PrimengModule } from './../modules/primeng/primeng.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MenuModule } from 'primeng/menu';

@NgModule({
  declarations: [
    BeneficiarioComponent,
    IdentificacaoComponent,
    VidaFuncionalComponent,
    ContagemTempoComponent,
    RemuneracaoProventosComponent,
    ManterBeneficioComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    PrimengModule,
    FlexLayoutModule,
    MenuModule
  ],
  exports: [
    BeneficiarioComponent,
    IdentificacaoComponent,
    VidaFuncionalComponent,
    ContagemTempoComponent,
    RemuneracaoProventosComponent,
    ManterBeneficioComponent
  ]
})
export class BeneficioModule { }
