import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VidaFuncionalComponent } from './vida-funcional.component';

describe('VidaFuncionalComponent', () => {
  let component: VidaFuncionalComponent;
  let fixture: ComponentFixture<VidaFuncionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VidaFuncionalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VidaFuncionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
