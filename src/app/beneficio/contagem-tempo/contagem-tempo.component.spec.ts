import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContagemTempoComponent } from './contagem-tempo.component';

describe('ContagemTempoComponent', () => {
  let component: ContagemTempoComponent;
  let fixture: ComponentFixture<ContagemTempoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContagemTempoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContagemTempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
