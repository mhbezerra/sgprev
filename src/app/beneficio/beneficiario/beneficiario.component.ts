import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-beneficiario',
  templateUrl: './beneficiario.component.html',
  styleUrls: ['./beneficiario.component.css']
})
export class BeneficiarioComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }

  formBeneficiario: FormGroup;

  ngOnInit(): void {
    this.initFormBeneficiario();
  }

  initFormBeneficiario() {
    this.formBeneficiario = this.formBuilder.group({
      nome: [null, [Validators.required]],
      cpf: [null, [Validators.required]],
      orgao: [null, [Validators.required]],
      matricula: [null, [Validators.required]]
    });
  }

}
