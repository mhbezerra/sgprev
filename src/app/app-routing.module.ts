import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BeneficioComponent } from './beneficio/beneficio.component';
import { ContagemTempoComponent } from './beneficio/contagem-tempo/contagem-tempo.component';
import { IdentificacaoComponent } from './beneficio/identificacao/identificacao.component';
import { ManterBeneficioComponent } from './beneficio/manter-beneficio/manter-beneficio.component';
import { RemuneracaoProventosComponent } from './beneficio/remuneracao-proventos/remuneracao-proventos.component';
import { VidaFuncionalComponent } from './beneficio/vida-funcional/vida-funcional.component';

const routes: Routes = [
  { path: '', 
    children: [
      // Redirecionamento default
      {
        path: '',
        redirectTo: '/beneficio',
        pathMatch: 'full'
      },
      
    ]
    , pathMatch: 'full'
  },
  {
    path: 'beneficio', component: ManterBeneficioComponent, pathMatch: 'full'
  },
  { path:'incluirBeneficio',component: BeneficioComponent, 
    children:[
      {path:'', redirectTo: 'beneficiario', pathMatch: 'full'},
      {path: 'beneficiario', component: BeneficioComponent},
      {path: 'identificacao', component: IdentificacaoComponent},
      {path: 'vida-funcional', component: VidaFuncionalComponent},
      {path: 'contagem-tempo', component: ContagemTempoComponent},
      {path: 'remuneracao-proventos', component: RemuneracaoProventosComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
